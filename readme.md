## Question 1.0
Design a flat function which flattens an array to any depth. Use the below array to test your funciton.

Example Input:

```javascript
[
    [
        "1a"
    ],
    [
        [
            "2a",
            "2a"
        ],
        [
            "2c"
        ]
    ],
    [
        "3a", 
        "3b",
        [
            [
                "3c",
                "3d"
            ]
        ]
    ]
]
```

The result should look like:

```javascript
    [
        "1a",
        "2a",
        "2b",
        "2c",
        "3a",
        "3b",
        "3c",
        "3d"
    ]
```


## Question 2.0

Write code to eliminate duplicate objects in an array where each object has an 'id' property which can be used to identify the object and the duplicate object with lower rank to be removed

```javascript
// Example
const arr = [
    {
        id: 1,
        name: 'emp1',
        rank: 4,
    },
    {
        id: 2,
        name: 'emp2',
        rank: 1,
    },
    {
        id: 2,
        name: 'emp2',
        rank: 2,                // this is a duplicate object (id = 2) and has lower rank 
    },
    {
        id: 3,
        name: 'emp3',
        rank: 3,
    },
];
```



